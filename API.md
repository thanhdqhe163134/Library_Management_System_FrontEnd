Books API:

1. List:
URL: /api/books
Method: GET
Description: Lấy danh sách tất cả các sách.

2. Detail:
URL: /api/books/{id}
Method: GET
Description: Lấy chi tiết của một sách dựa trên ID của sách.

3. Search:
URL: /api/books/search?keyword={keyword}
Method: GET
Description: Tìm kiếm sách dựa trên từ khóa. Từ khóa có thể là title, author, hoặc publisher.\

4. Delete:
URL: /api/books/{id}
Method: DELETE
Description: Xóa một sách dựa trên ID của sách.

5. Restore:
URL: /api/books/restore/{id}
Method: PUT
Description: Khôi phục một sách đã bị xóa dựa trên ID của sách

6. Edit:
URL: /api/books/{id}
Method: PUT
Description: Chỉnh sửa thông tin của một sách. Cần truyền tất cả thông tin của sách, kể cả thông tin không thay đổi.

7. Add:
URL: /api/books
Method: POST
Description: Thêm một quyển sách mới. Cần truyền title, author, publisher, total_copies, description, và img. Các thông tin khác sẽ 
được xử lý ở BE.

Comments API:

1. List All Comments for a Book:
URL: /api/comments/{book_id}
Method: GET
Description: Lấy danh sách tất cả các bình luận cho một quyển sách dựa trên book_id.

2. Add Comment:
URL: /api/comments/
Method: POST
Description: Thêm một bình luận mới. Cần truyền hết thông tin vào

3. Edit Comment:
URL: /api/comments/{comment_id}
Method: PUT
Description: Chỉnh sửa một bình luận dựa trên comment_id. Cần truyền content mới.

4. Delete Comment:
URL: /api/comments/{comment_id}
Method: DELETE
Description: Xóa một bình luận dựa trên comment_id.