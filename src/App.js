import { Link, Route, Routes } from 'react-router-dom';
import BookList from './book/BookList';
import BookDetail from './book/BookDetail';
import BookAdd from './book/BookAdd';
import Login from './account/Login';
import AccountsList from './account/AccountsList'; 
import { useAccount } from './account/AccountContext';
import Profile from './account/Profile';

function App() {
  const { currentUser, logout } = useAccount();

  return (
    <div className="App">
      <h1>Library Management System</h1>
      {currentUser ? (
        <>
          <p>Welcome, {currentUser.username}</p>
          <p>Role: {currentUser.role} </p>
          <button onClick={logout}>Logout</button>
        </>
      ) : (
        <Link to="/login">Login</Link>
      )}
      <nav>
        <ul>
          <li><Link to="/book-list">View Book List</Link></li>
          {currentUser && currentUser.role === 'Admin' && (
          <li><Link to="/accounts-list">View Accounts List</Link></li>
          )}
          {currentUser && currentUser.role  && (
          <li><Link to={`/profile/${currentUser.username}`}>View Profile</Link></li>
          )}

        </ul>
      </nav>
      <Routes>
        <Route path="/book-list" element={<BookList />} />
        <Route path="/book-detail/:id" element={<BookDetail />} />
        <Route path="/book-add" element={<BookAdd />} />
        <Route path="/login" element={<Login />} />
        <Route path="/accounts-list" element={<AccountsList />} />
        <Route path="/profile/:username" element={<Profile />} />

      </Routes>
    </div>
  );
}

export default App;
