import React from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';
import './index.css';
import App from './App';
import BookList from './book/BookList';
import Login from './account/Login';
import BookDetail from './book/BookDetail';
import BookAdd from './book/BookAdd';
import AccountsList from './account/AccountsList';
import { createRoot } from 'react-dom';
import axios from 'axios';
import { AccountProvider } from './account/AccountContext';
import Profile from './account/Profile';

axios.defaults.baseURL = 'http://localhost:8080';

const root = createRoot(document.getElementById('root'));

root.render(
  <React.StrictMode>
    <Router>
      <AccountProvider>
        <Routes>
          <Route path="/" element={<Navigate to="/home" />} />
          <Route path="/home" element={<App />} />
          <Route path="/book-list" element={<BookList />} />
          <Route path="/login" element={<Login />} />
          <Route path="/book-detail/:id" element={<BookDetail />} />
          <Route path="/book-add" element={<BookAdd />} />
          <Route path="/accounts-list" element={<AccountsList />} />
          <Route path="/profile/:username" element={<Profile />} />
        </Routes>
      </AccountProvider>
    </Router>
  </React.StrictMode>
);