import { createContext, useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';


const AccountContext = createContext();

export const useAccount = () => {
  return useContext(AccountContext);
};

export const AccountProvider = ({ children }) => {
  const [currentUser, setCurrentUser] = useState(null);
  const navigate = useNavigate();
  const logout = () => {
    setCurrentUser(null);
    localStorage.removeItem('user');
    navigate('/home');

    
  };
  useEffect(() => {
    const user = localStorage.getItem('user');
    if (user) {
      setCurrentUser(JSON.parse(user));
    }
    }, []);

  return (
    <AccountContext.Provider value={{ currentUser, setCurrentUser, logout }}>
      {children}
    </AccountContext.Provider>
  );
};