import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import { useAccount } from './AccountContext';
import './Profile.css';

const Profile = () => {
  const [account, setAccount] = useState(null);
  const [editMode, setEditMode] = useState(false);
  const [imageFile, setImageFile] = useState(null);
  const [editedAccount, setEditedAccount] = useState(null);
  const { username } = useParams();
  const { currentUser } = useAccount();

  useEffect(() => {
    const fetchProfile = async () => {
      try {
        const token = currentUser?.jwt;
        const response = await axios.get(`/api/accounts/${username}`, {
          headers: {
            Authorization: `Bearer ${token}`
          }
        });
        setAccount(response.data);
        setEditedAccount({ ...response.data });
      } catch (error) {
        console.error('Error fetching profile:', error);
      }
    };
    fetchProfile();
  }, [username, currentUser]);

  const handleEditMode = () => {
    setEditMode(true);
  };

  const handleSaveChanges = async () => {
    if (imageFile) {
      const formData = new FormData();
      formData.append('image', imageFile);
      try {
        const response = await axios.post('/api/images/upload', formData, {
          headers: { 'Content-Type': 'multipart/form-data' }
        });
        editedAccount.img = response.data.data.link;
      } catch (error) {
        console.error('Error uploading image:', error);
      }
    }

    try {
      const token = currentUser?.jwt;
      await axios.put(`/api/accounts/${username}`, editedAccount, {
        headers: { Authorization: `Bearer ${token}` }
      });
      setEditMode(false);
      window.location.reload();
    } catch (error) {
      console.error('Error updating account:', error);
    }
  };

  const handleImageChange = (e) => {
    setImageFile(e.target.files[0]);
  };

  const handleCancelEditMode = () => {
    setEditMode(false);
  };

  if (!account) return <p>Loading...</p>;

  return (
    <div className="profile-container">
      {editMode ? (
        <>
          <label>Username:
            <input type="text" value={editedAccount.username} readOnly />
          </label>
          <label>Full Name:
            <input type="text" value={editedAccount.fullName} onChange={(e) => setEditedAccount({ ...editedAccount, fullName: e.target.value })} />
          </label>
          <label>Email:
            <input type="email" value={editedAccount.email} onChange={(e) => setEditedAccount({ ...editedAccount, email: e.target.value })} />
          </label>
          <label>Phone:
            <input type="tel" value={editedAccount.phoneNumber} onChange={(e) => setEditedAccount({ ...editedAccount, phoneNumber: e.target.value })} />
          </label>
          <label>Address:
            <input type="text" value={editedAccount.address} onChange={(e) => setEditedAccount({ ...editedAccount, address: e.target.value })} />
          </label>
          {currentUser && currentUser.role === 'Admin' && (
            <label>Role:
              <select value={editedAccount.role} onChange={(e) => setEditedAccount({ ...editedAccount, role: e.target.value })}>
                <option value="User">User</option>
                <option value="Librarian">Librarian</option>
                <option value="Admin">Admin</option>
              </select>
            </label>
          )}
          <label>Image:
            <input type="file" onChange={handleImageChange} />
          </label>
          {editedAccount.img && <img src={editedAccount.img} alt="Edited" />}
          <button onClick={handleSaveChanges}>Save Changes</button>
          <button onClick={handleCancelEditMode}>Cancel</button>
        </>
      ) : (
        <>
          <img src={account.img} alt={account.username} />
          <p>Username: {account.username}</p>
          <p>Full Name: {account.fullName}</p>
          <p>Email: {account.email}</p>
          <p>Phone: {account.phoneNumber}</p>
          <p>Address: {account.address}</p>
          {currentUser && currentUser.role === 'Admin' && (
            <p>Role: {account.role}</p>
          )}
          <p>Created Date: {account.createdDate}</p>
          <p>Updated Date: {account.updatedDate}</p>
          {currentUser && <button onClick={handleEditMode}>Edit Profile</button>}
        </>
      )}
    </div>
  );
};

export default Profile;
