import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { useAccount } from './AccountContext';
import './AccountsList.css'; 

const AccountsList = () => {
  const [accounts, setAccounts] = useState([]);
  const { currentUser } = useAccount(); // Dùng để lấy token

  useEffect(() => {
    const fetchAccounts = async () => {
      try {
        const token = currentUser?.jwt; // Lấy token từ context
        const response = await axios.get('/api/accounts', {
          headers: {
            Authorization: `Bearer ${token}`
          }
        });
        setAccounts(response.data); // Gán dữ liệu từ server vào state
      } catch (error) {
        console.error('Error fetching accounts:', error);
      }
    };

    fetchAccounts(); // Gọi hàm để lấy dữ liệu khi component được mount
  }, [currentUser]);

  return (
    <div className="accounts-container">
      <h1>Accounts List</h1>
      <div className="accounts-list">
        {accounts.map(account => (
          <div key={account.username} className="account-item">
            <p><strong>Username:</strong> {account.username}</p>
            <p><strong>Full Name:</strong> {account.fullName}</p>
            <p><strong>Email:</strong> {account.email}</p>
            <p><strong>Phone:</strong> {account.phoneNumber}</p>
            <p><strong>Address:</strong> {account.address}</p>
            <p><strong>Role:</strong> {account.role}</p>
            <p><strong>Created Date:</strong> {account.createdDate}</p>
            <p><strong>Updated Date:</strong> {account.updatedDate}</p>
            <br />
            <br></br>
            <Link to={`/profile/${account.username}`} className="detail-button">View Detail</Link>
            <button className="delete-button">Delete</button>
          </div>
        ))}
      </div>
    </div>
  );
};

export default AccountsList;
