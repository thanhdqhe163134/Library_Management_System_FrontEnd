import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom'; // Import useNavigate
import { useAccount } from './AccountContext';
import './Login.css';

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const { setCurrentUser } = useAccount();
  const navigate = useNavigate(); // Use useNavigate to get the navigate function

  const handleLogin = async () => {
    try {
      const response = await axios.post('/api/accounts/login', { username, password });
      setCurrentUser(response.data);
      localStorage.setItem('user', JSON.stringify(response.data));
      navigate('/home'); // Navigate to /home on successful login
    } catch (error) {
      console.error('Login error', error);
    }
  };

  return (
    <div className="login-container">
      <input type="text" value={username} onChange={(e) => setUsername(e.target.value)} placeholder="username" />
      <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} placeholder="password" />
      <button onClick={handleLogin}>Login</button>
    </div>
  );
};

export default Login;