import React, { useState, useEffect } from 'react';
import { useParams, useNavigate   } from 'react-router-dom';
import axios from 'axios';
import './BookDetail.css';
import { useAccount } from '../account/AccountContext';


function BookDetail() {
  const { id } = useParams();
  const [book, setBook] = useState(null);
  const [editMode, setEditMode] = useState(false);
  const [imageFile, setImageFile] = useState(null);
  const [editedBook, setEditedBook] = useState(null);
  const [selectedGenres, setSelectedGenres] = useState([]);
  const [newGenre, setNewGenre] = useState('');
  const [genres, setGenres] = useState([]);
  const [comments, setComments] = useState([]);
  const { currentUser } = useAccount();
  const navigate = useNavigate();



  useEffect(() => {
    const fetchDetails = async () => {
      try {
        const bookResponse = await axios.get(`/api/books/${id}`);
        setBook(bookResponse.data);
        setEditedBook({ ...bookResponse.data });
        setSelectedGenres(Array.isArray(bookResponse.data.genres) ? bookResponse.data.genres : []);
        const genresResponse = await axios.get('/api/books/genres');
        setGenres(genresResponse.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchDetails();

    const fetchComments = async () => {
      try {
          const response = await axios.get(`/api/comments/${id}`);
          if (response.status === 200) {
              setComments(response.data);
          }
      } catch (error) {
          console.error('Error fetching comments:', error);
      }
  };
  fetchComments();

  }, [id]);


  const handleEditMode = () => {
    if (book && Array.isArray(book.genres)) {
        setSelectedGenres(book.genres);
    }
    setEditMode(true);
  };


  const handleSaveChanges = async () => {
    if (imageFile) {
        const formData = new FormData();
        formData.append('image', imageFile);
        
        try {
            const response = await axios.post('/api/images/upload', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
            });
            editedBook.img = response.data.data.link;
        } catch (error) {
            console.error('Error uploading image:', error);
        }
    }

    try {
      const user = JSON.parse(localStorage.getItem('user'));
      const token = user?.jwt;
      await axios.put(`/api/books/${id}`, { ...editedBook, genres: selectedGenres }, {
          headers: {
              Authorization: `Bearer ${token}`
          }
      });
      
      setEditMode(false);
      window.location.reload(); 
    } catch (error) {
        console.error('Error updating book:', error);
    }
};

  const handleImageChange = (e) => {
    setImageFile(e.target.files[0]);
  };

  const handleGenreClick = (genre) => {
    setSelectedGenres([...selectedGenres, genre]);
  };

  const handleGenreClick2 = (genre) => {
    navigate(`/book-list?genre=${genre}`);
  };

  const handleRemoveSelectedGenre = (genre) => {
    setSelectedGenres(selectedGenres.filter(g => g !== genre));
  };

  const handleAddGenreClick = () => {
    if (newGenre && !genres.includes(newGenre) && !selectedGenres.includes(newGenre)) {
        setSelectedGenres(prevSelectedGenres => [...prevSelectedGenres, newGenre]);
        setNewGenre('');
    }
};

const handleCancelEditMode = () => {
  if (book && Array.isArray(book.genres)) {
      setSelectedGenres(book.genres); // Thiết lập lại selectedGenres khi hủy chỉnh sửa
  }
  setEditMode(false);
};




if (!book) return <div>Loading...</div>;

  return (
    <div className="book-detail">
      {editMode ? (
        <>
          <label>Title:
            <input type="text" value={editedBook.title} onChange={(e) => setEditedBook({ ...editedBook, title: e.target.value })} />
          </label>
          <label>Author:
            <input type="text" value={editedBook.author} onChange={(e) => setEditedBook({ ...editedBook, author: e.target.value })} />
          </label>
          <label>Publisher:
            <input type="text" value={editedBook.publisher} onChange={(e) => setEditedBook({ ...editedBook, publisher: e.target.value })} />
          </label>
          <label>Total Copies:
            <input type="number" value={editedBook.total_copies} onChange={(e) => setEditedBook({ ...editedBook, total_copies: e.target.value })} />
          </label>
          <label>Description:
            <textarea value={editedBook.description} onChange={(e) => setEditedBook({ ...editedBook, description: e.target.value })} />
          </label>
          <label>Image:
            <input type="file" onChange={handleImageChange} />
        </label>
        {editedBook.img && <img src={editedBook.img} alt="Edited" />}
          <div className="genres">
            <h3>Genres</h3>
            {genres.map((genre) => (
              !selectedGenres.includes(genre) &&
              <button key={genre} onClick={() => handleGenreClick(genre)}>{genre}</button>
            ))}
          </div>
          <div className="new-genre">
            <input type="text" value={newGenre} onChange={(e) => setNewGenre(e.target.value)} placeholder="New Genre" />
            <button onClick={handleAddGenreClick}>+</button>
          </div>
          <div className="selected-genres">
            <h3>Selected Genres</h3>
            {selectedGenres.map(genre => (
              <div key={genre} className="selected-genre">
                <span>{genre}</span>
                <button onClick={() => handleRemoveSelectedGenre(genre)}>Remove</button>
              </div>
            ))}
          </div>
          <button onClick={handleSaveChanges}>Save Changes</button>
          <button onClick={handleCancelEditMode}>Cancel</button>
        </>
     ) : (
      <>
        <img src={book.img} alt={book.title} />
        <h2>{book.title}</h2>
        <p><strong>Author:</strong> {book.author}</p>
        <p><strong>Publisher:</strong> {book.publisher}</p>
        <p><strong>Total Copies:</strong> {book.total_copies}</p>
        <p><strong>Copies Borrowed:</strong> {book.copies_borrowed}</p>
        <p><strong>Copies Available:</strong> {book.copies_available}</p>
        <p><strong>Description:</strong> {book.description}</p>
        <p>
        {book.genres && book.genres.map((genre) => (
          <button 
            key={genre} 
            className="genre-button"
            onClick={() => handleGenreClick2(genre)} 
          >
            {genre}
          </button>
        ))}
      </p>
        {currentUser && currentUser.role === 'Admin' && (
        <button onClick={handleEditMode}>Edit Book</button>
        )}
      </>
    )}
    <div className="comments-section">
    <h3>Comments</h3>
    {comments.map((comment) => (
        <div 
            key={comment.comment_id} 
            className={`comment ${comment.rep_comment > 0 ? 'reply' : ''}`}
        >
            <p><strong>{comment.created_user}:</strong> {comment.content}</p>
        </div>
    ))}
</div>
  </div>
);
}

export default BookDetail;