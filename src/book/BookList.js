import React, { useState, useEffect  } from 'react';
import { Link } from 'react-router-dom';
import { useLocation } from 'react-router-dom';
import './BookList.css';
import axios from 'axios';
import { useAccount } from '../account/AccountContext';


const BookList = () => {
    const [books, setBooks] = useState([]);
    const [genres, setGenres] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');
    const [selectedGenre, setSelectedGenre] = useState('');
    const { currentUser } = useAccount();
    const location = useLocation(); 




    useEffect(() => {
        const fetchGenres = async () => {
            try {
                const response = await axios.get('/api/books/genres');
                if (response.status === 200) {
                    setGenres(response.data);
                }
            } catch (error) {
                console.error("An error occurred while fetching the genres", error);
            }
        };
        
        fetchGenres();
    }, []);

    useEffect(() => {
        const fetchBooks = async () => {
            try {
                let url = '/api/books';
                if (searchTerm) {
                    url = `/api/books/search?keyword=${searchTerm}`;
                } else if (selectedGenre) {
                    url = `/api/books/searchByGenre?genre=${selectedGenre}`;
                }
                const response = await axios.get(url);
                if (response.status === 200) {
                    setBooks(response.data);
                }
            } catch (error) {
                console.error("An error occurred while fetching the books", error);
            }
        };
        
        const timerId = setTimeout(() => {
            fetchBooks();
        }, 500);

        return () => {
            clearTimeout(timerId);
        };
    }, [searchTerm, selectedGenre]);

    useEffect(() => {
        // Lấy selectedGenre từ query param của URL
        const params = new URLSearchParams(location.search);
        const genre = params.get('genre');
        if (genre) setSelectedGenre(genre);
      }, [location.search]);

    const handleGenreClick = (genre) => {
        setSearchTerm(''); // Clear the search term
        setSelectedGenre(genre); // Set the selected genre
    };

    const handleDeleteGenre = (genre) => {
        if(!window.confirm(`Are you sure you want to delete genre: ${genre}?`)) {
            return;
        }
        
        const user = JSON.parse(localStorage.getItem('user'));
        const token = user?.jwt;
        console.log(genre);

        axios.delete(`/api/books/genres/${genre}`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        
        .then((response) => {
            console.log(response.data);
            setGenres(genres.filter(g => g !== genre));
            window.location.reload()
        })
        .catch((error) => console.error('Error deleting genre:', error));
    };
    

    const handleDeleteBook = (book_id) => {
        if(!window.confirm(`Bạn có chắc muốn xóa cuốn sách này?`)) {
            return;
        }
        // Lấy token từ localStorage hoặc context
        const user = JSON.parse(localStorage.getItem('user'));
        const token = user?.jwt;
    
        axios.delete(`/api/books/${book_id}`, {
            headers: {
                // Thiết lập token vào header Authorization
                Authorization: `Bearer ${token}`
            }
        })
        .then((response) => {
            // Cập nhật danh sách sách sau khi xóa thành công
            setBooks(books.filter(book => book.book_id !== book_id));
            console.log('Book deleted:', response.data);
        })
        .catch((error) => console.error('Error deleting book:', error));
    };
    

    return (
        <div className="book-list">
            <input 
                type="text" 
                placeholder="Search..."
                onChange={(e) => {
                    setSearchTerm(e.target.value);
                    setSelectedGenre(''); // Clear the selected genre when the user starts typing a search term
                }}
                value={searchTerm}
            />

                {currentUser && currentUser.role === 'Admin' && (
                <div>
                <Link to={`/book-add`} className="view-details-button">Add a new book</Link>
                </div> 
                )}        
            <div className="genre-list">
    <button onClick={() => setSelectedGenre('')}>All</button>
    {genres.map((genre) => (
        <span key={genre}>
            <button onClick={() => handleGenreClick(genre)}>{genre}</button>
            {currentUser && currentUser.role === 'Admin' && (
                <button onClick={() => handleDeleteGenre(genre)} className='delete-genre-button'>X</button>
            )}
        </span>
    ))}
</div>

            {books.map((book) => (
                <div key={book.bookId} className="book">
                    {currentUser && currentUser.role === 'Admin' && (
                    <button onClick={() => handleDeleteBook(book.book_id)} className="delete-button">Delete</button>
                    )}
                    <br></br>
                    <img src={book.img} alt={book.title} style={{ width: '150px', height: '200px' }} />
                    <h3>Title: {book.title}</h3>
                    <p>Author: {book.author}</p>
                    <p>Publisher: {book.publisher}</p>
                    <p>{book.description}</p>
                    <p>Copies Available: {book.copies_available}</p>
                    <div className="genre-buttons">
                        {book.genres && book.genres.map((genre) => (
                            <button key={genre} className="genre-button" onClick={() => handleGenreClick(genre)}>
                                {genre}
                            </button>
                        ))}
                    </div>
                    <Link to={`/book-detail/${book.book_id}`} className="view-details-button">View Details</Link>
                </div>
            ))}
        </div>
    );
};

export default BookList;