import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import './BookAdd.css';
import  '../account/AccountContext';



function BookAdd() {
    const [title, setTitle] = useState('');
    const [author, setAuthor] = useState('');
    const [publisher, setPublisher] = useState('');
    const [totalCopies, setTotalCopies] = useState('');
    const [description, setDescription] = useState('');
    const [img, setImg] = useState('');
    const [genres, setGenres] = useState([]);
    const [selectedGenres, setSelectedGenres] = useState([]);
    const [newGenre, setNewGenre] = useState('');
    const navigate = useNavigate();
    const [imageFile, setImageFile] = useState(null);




    useEffect(() => {
        axios.get('/api/books/genres')
            .then(response => setGenres(response.data))
            .catch(error => console.error('Error fetching genres:', error));
    }, []);

    const handleGenreClick = (genre) => {
        setSelectedGenres([...selectedGenres, genre]);
    };

    const handleAddGenreClick = () => {
        if (newGenre && !genres.includes(newGenre) && !selectedGenres.includes(newGenre)) {
            setSelectedGenres([...selectedGenres, newGenre]);
            setNewGenre('');
        }
    };

    const handleRemoveSelectedGenre = (genre) => {
      setSelectedGenres(selectedGenres.filter(g => g !== genre));
  };

  const handleSubmit = async () => {
    const imageUrl = await handleUploadImage();

    if (imageUrl) {
        setImg(imageUrl);
    }

    const bookData = {
        title,
        author,
        publisher,
        total_copies: totalCopies,
        description,
        img: imageUrl ? imageUrl : img, 
        genres: selectedGenres
    };

    try {
        const user = JSON.parse(localStorage.getItem('user'));
        const token = user?.jwt;
        await axios.post('/api/books', bookData, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        console.log('Book added');
        navigate('/book-list');
    } catch (error) {
        console.error('Error adding book:', error);
    }
    return null;
};

  const handleImageChange = (e) => {
    setImageFile(e.target.files[0]);
};

const handleUploadImage = async () => {
  if (imageFile) {
      const formData = new FormData();
      formData.append('image', imageFile);
      
      try {
          const response = await axios.post('/api/images/upload', formData, {
              headers: {
                  'Content-Type': 'multipart/form-data',
              },
          });
          const imageUrl = response.data.data.link;
          return imageUrl;
      } catch (error) {
          console.error('Error uploading image:', error);
      }
  }
  return null;
};

return (
  <div className="book-add">
      <h1>Add Book</h1>
      <div className="form-group">
          <label>Title: </label>
          <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
      </div>
      <div className="form-group">
          <label>Author: </label>
          <input type="text" value={author} onChange={(e) => setAuthor(e.target.value)} />
      </div>
      <div className="form-group">
          <label>Publisher: </label>
          <input type="text" value={publisher} onChange={(e) => setPublisher(e.target.value)} />
      </div>
      <div className="form-group">
          <label>Total Copies: </label>
          <input type="number" value={totalCopies} onChange={(e) => setTotalCopies(e.target.value)} />
      </div>
      <div className="form-group">
          <label>Description: </label>
          <textarea value={description} onChange={(e) => setDescription(e.target.value)} />
      </div>
      <div className="form-group">
          <label>Image: </label>
          <input type="file" onChange={handleImageChange} />
      </div>
      <div className="genres">
          <h3>Genres</h3>
          <div className="available-genres">
              {genres.map((genre) => (
                  !selectedGenres.includes(genre) && 
                  <button key={genre} onClick={() => handleGenreClick(genre)}>{genre}</button>
              ))}
          </div>
          <div className="new-genre">
              <input type="text" value={newGenre} onChange={(e) => setNewGenre(e.target.value)} placeholder="New Genre" />
              <button onClick={handleAddGenreClick}>+</button>
          </div>
      </div>
      <div className="selected-genres">
          <h3>Selected Genres</h3>
          {selectedGenres.map(genre => (
              <div key={genre} className="selected-genre">
                  <span>{genre}</span>
                  <button onClick={() => handleRemoveSelectedGenre(genre)}>Remove</button>
              </div>
          ))}
      </div>
      <button onClick={handleSubmit} className="submit-button">Submit</button>
  </div>
);
}

export default BookAdd;